import React, {useEffect} from "react";
import {useSelector} from "react-redux";
import {BrowserRouter, Routes, Route, Navigate} from "react-router-dom";
import {GoogleOAuthProvider} from "@react-oauth/google";

import Protected from "./components/Protected";
import Homepage from "./pages/Homepage"; // Homepage
import Login from "./pages/Login"; // Login
import Register from "./pages/Register"; // Register
import UpdateInfoAkun from "./pages/InfoAkun"; // UpdateInfoAkun
import InfoProduct from "./pages/InfoProduk"; // InfoAkun
import AddProduct from "./pages/AddProduct"; // AddProduct
import EditProduct from "./pages/EditProduct"; // EditProduct
import DaftarJual from "./pages/DaftarJual"; // DaftarJual
import Transaksi from "./pages/Transaksi"; // Transaksi
import HalamanProduct from "./pages/BuyerHalamanProduct"; // HalamanProduct

const {REACT_APP_CLIENT_ID} = process.env;

const App = () => {
    const theme = useSelector((state) => state.theme);

    useEffect(() => {
        (async () => {
            await import("./public/css/style.css");
            await import("./public/css/style-dark.css");

            const style = document.getElementsByTagName("style");
            let styleLength = style.length;
            if (theme.theme === "dark") {
                style[styleLength - 1].disabled = false;
            } else if (theme.theme === "light") {
                style[styleLength - 1].disabled = true;
            }
        })();
    }, [theme]);

    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Homepage />} />
                <Route
                    path="/login"
                    element={
                        <GoogleOAuthProvider clientId={REACT_APP_CLIENT_ID}>
                            <Login />
                        </GoogleOAuthProvider>
                    }
                />
                <Route
                    path="/register"
                    element={
                        <GoogleOAuthProvider clientId={REACT_APP_CLIENT_ID}>
                            <Register />
                        </GoogleOAuthProvider>
                    }
                />
                <Route
                    path="/tambah-produk"
                    element={
                        <Protected>
                            <AddProduct />
                        </Protected>
                    }
                />
                <Route
                    path="/info-produk/:id"
                    element={
                        <Protected>
                            <InfoProduct />
                        </Protected>
                    }
                />
                <Route
                    path="/edit-produk/:id"
                    element={
                        <Protected>
                            <EditProduct />
                        </Protected>
                    }
                />
                <Route
                    path="/daftar-jual"
                    element={
                        <Protected>
                            <DaftarJual />
                        </Protected>
                    }
                />

                <Route
                    path="/info-akun"
                    element={
                        <Protected>
                            <UpdateInfoAkun />
                        </Protected>
                    }
                />

                <Route
                    path="/product/:id"
                    element={
                        <Protected>
                            <HalamanProduct />
                        </Protected>
                    }
                />

                <Route
                    path="/transaksi"
                    element={
                        <Protected>
                            <Transaksi />
                        </Protected>
                    }
                />

                {/* 404 Page redirect to path "/" */}
                <Route path="*" element={<Navigate to={"/"} />} />
            </Routes>
        </BrowserRouter>
    );
};

export default App;
