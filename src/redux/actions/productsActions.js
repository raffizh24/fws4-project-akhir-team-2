import Swal from "sweetalert2";
import {GET_ALL_PRODUCT, GET_PRODUCT, CREATE_PRODUCT, UPDATE_PRODUCT, PREVIEW_PROODUCT, CLEAR_PRODUCT, PRODUCT_ERROR, DELETE_PRODUCT} from "./types";

const {REACT_APP_BACKEND} = process.env;

export const getAllProducts = () => async (dispatch) => {
    let token = "";
    if (localStorage.getItem("token")) token = `Bearer ${localStorage.getItem("token")}`;

    try {
        const response = await fetch(REACT_APP_BACKEND + "/api/v1/products", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: token,
            },
        });
        const data = await response.json();

        if (response.status === 500) {
            dispatch({
                type: PRODUCT_ERROR,
                payload: data.message,
            });
        } else {
            dispatch({
                type: GET_ALL_PRODUCT,
                payload: data,
                status: "GET_ALL",
            });
        }
    } catch (error) {
        dispatch({
            type: PRODUCT_ERROR,
            payload: error.response,
        });
        Swal.fire({
            position: "center",
            icon: "error",
            title: error.message,
            showConfirmButton: false,
            timer: 1500,
        });
    }
};

export const getProductById = (params) => async (dispatch) => {
    try {
        const id = params;
        const response = await fetch(REACT_APP_BACKEND + `/api/v1/product/${id}`, {
            method: "GET",
        });
        const data = await response.json();

        dispatch({
            type: GET_PRODUCT,
            payload: data,
        });
    } catch (error) {
        dispatch({
            type: PRODUCT_ERROR,
            payload: error.response,
        });
        Swal.fire({
            position: "center",
            icon: "error",
            title: error.message,
            showConfirmButton: false,
            timer: 1500,
        });
    }
};

export const getProductByNama = (params) => async (dispatch) => {
    try {
        const namaProduk = params;
        const response = await fetch(
            REACT_APP_BACKEND +
                "/api/v1/product/filter/nama?" +
                new URLSearchParams({
                    namaProduk,
                })
        );
        const data = await response.json();

        dispatch({
            type: GET_ALL_PRODUCT,
            payload: data,
            status: "GET_ALL",
        });
    } catch (error) {
        dispatch({
            type: PRODUCT_ERROR,
            payload: error.response,
        });

        Swal.fire({
            position: "center",
            icon: "error",
            title: error.message,
            showConfirmButton: false,
            timer: 1500,
        });
    }
};

export const getProductByKategori = (params) => async (dispatch) => {
    let token = "";
    if (localStorage.getItem("token")) token = `Bearer ${localStorage.getItem("token")}`;

    try {
        const response = await fetch(REACT_APP_BACKEND + "/api/v1/product/filter/kategori?" + new URLSearchParams({kategori: params}), {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: token,
            },
        });
        const data = await response.json();

        dispatch({
            type: GET_ALL_PRODUCT,
            payload: data,
            status: "GET_ALL",
        });
    } catch (error) {
        dispatch({
            type: PRODUCT_ERROR,
            payload: error.response,
        });

        Swal.fire({
            position: "center",
            icon: "error",
            title: error.message,
            showConfirmButton: false,
            timer: 1500,
        });
    }
};

export const getProductByStatus = (params) => async (dispatch) => {
    try {
        const {idSeller, statusProduk} = params;

        const response = await fetch(
            REACT_APP_BACKEND +
                "/api/v1/product/filter/status?" +
                new URLSearchParams({
                    idSeller,
                    statusProduk,
                })
        );
        const data = await response.json();

        dispatch({
            type: GET_ALL_PRODUCT,
            payload: data,
            status: "GET_ALL",
        });
    } catch (error) {
        dispatch({
            type: PRODUCT_ERROR,
            payload: error.response,
        });
        Swal.fire({
            position: "center",
            icon: "error",
            title: error.message,
            showConfirmButton: false,
            timer: 1500,
        });
    }
};

export const addProduct = (params) => async (dispatch) => {
    try {
        var formdata = new FormData();
        formdata.append("namaProduk", params.namaProduk);
        formdata.append("hargaProduk", params.harga);
        formdata.append("kategori", params.kategori);
        formdata.append("deskripsi", params.deskripsi);

        for (var i = 0; i < params.fotoProduk.length; i++) {
            if (params.fotoProduk[i] !== "") {
                formdata.append("fotoProduk", params.fotoProduk[i]);
            }
        }

        const response = await fetch(REACT_APP_BACKEND + "/api/v1/product", {
            method: "POST",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
            body: formdata,
        });

        const result = await response.json();

        dispatch({
            type: CREATE_PRODUCT,
            status: result.status,
        });

        Swal.fire({
            position: "center",
            icon: "success",
            title: "Success",
            showConfirmButton: false,
            timer: 1500,
        });
    } catch (error) {
        dispatch({
            type: PRODUCT_ERROR,
            payload: error.response,
        });

        Swal.fire({
            position: "center",
            icon: "error",
            title: error,
            showConfirmButton: false,
            timer: 1500,
        });
    }
};

export const updateProduct = (params) => async (dispatch) => {
    try {
        var formdata = new FormData();
        formdata.append("idSeller", params.idSeller);
        formdata.append("namaProduk", params.namaProduk);
        formdata.append("hargaProduk", params.hargaProduk);
        formdata.append("kategori", params.kategori);
        formdata.append("deskripsi", params.deskripsi);
        formdata.append("statusProduk", params.statusProduk);

        // Delete old image
        if (params.oldImage.length !== 0 || params.oldImage !== []) {
            if (Array.isArray(params.oldImage)) {
                for (var x = 0; x < params.oldImage.length; x++) {
                    formdata.append("oldImage", params.oldImage[x]);
                }
            } else {
                formdata.append("oldImage", params.oldImage);
            }
        }

        // Upload new image
        for (var i = 0; i < params.fotoProduk.length; i++) {
            if (params.fotoProduk[i] !== "") {
                formdata.append("fotoProduk", params.fotoProduk[i]);
            }
        }

        const response = await fetch(REACT_APP_BACKEND + `/api/v1/product/${params.id}`, {
            method: "PUT",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
            body: formdata,
        });

        const data = await response.json();

        dispatch({
            type: UPDATE_PRODUCT,
            payload: data.status,
        });

        Swal.fire({
            position: "center",
            icon: "success",
            title: "Success",
            showConfirmButton: false,
            timer: 1500,
        });
    } catch (error) {
        dispatch({
            type: PRODUCT_ERROR,
            payload: error.response,
        });

        Swal.fire({
            position: "center",
            icon: "error",
            title: error,
            showConfirmButton: false,
            timer: 1500,
        });
    }
};

export const deleteProduct = (params) => async (dispatch) => {
    const {id, oldImage} = params;
    try {
        const response = await fetch(REACT_APP_BACKEND + "/api/v1/product?" + new URLSearchParams({id, oldImage}), {
            method: "DELETE",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
        });

        const data = await response.json();

        dispatch({
            type: DELETE_PRODUCT,
            payload: data.status,
        });

        Swal.fire({
            position: "center",
            icon: "success",
            title: "Delete success",
            showConfirmButton: false,
            timer: 1500,
        });
    } catch (error) {
        dispatch({
            type: PRODUCT_ERROR,
            payload: error,
        });

        Swal.fire({
            position: "center",
            icon: "error",
            title: error,
            showConfirmButton: false,
            timer: 1500,
        });
    }
};

export const clearProduct = () => async (dispatch) => {
    dispatch({
        type: CLEAR_PRODUCT,
    });
};

export const previewImg = (params) => async (dispatch) => {
    dispatch({
        type: PREVIEW_PROODUCT,
        payload: params,
    });
};
