import {combineReducers} from "redux";

import authReducer from "./authReducer";
import productReducer from "./productReducer";
import transactionReducer from "./transactionReducer";
import themeReducer from "./themeReducer";

export default combineReducers({
    auth: authReducer,
    product: productReducer,
    transaction: transactionReducer,
    theme: themeReducer,
});
