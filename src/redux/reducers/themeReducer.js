import {UPDATE_THEME} from "../actions/types";

const initialState = {
    theme: localStorage.getItem("theme") || "light",
};

const themeReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_THEME:
            return {
                ...state,
                theme: action.payload,
            };
        default:
            return state;
    }
};

export default themeReducer;
