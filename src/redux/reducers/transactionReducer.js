import {GET_ALL_TR_BUYER, GET_ALL_TR_SELLER, CREATE_TR, UPDATE_TR} from "../actions/types";

const initialState = {
    transactionSeller: [],
    transactionBuyer: [],
    status: null,
};

const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_TR_SELLER:
            return {
                ...state,
                transactionSeller: action.transaction,
                status: action.status,
            };
        case GET_ALL_TR_BUYER:
            return {
                ...state,
                transactionBuyer: action.transaction,
                status: action.status,
            };
        case CREATE_TR:
            return {
                ...state,
                detailTransaction: action.payload,
                status: action.status,
            };
        case UPDATE_TR:
            return {
                ...state,
                status: action.payload,
            };
        default:
            return state;
    }
};

export default productReducer;
