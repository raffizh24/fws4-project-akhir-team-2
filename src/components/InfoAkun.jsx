import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Container, Row, Col, Form, Button} from "react-bootstrap";
import {Link, Navigate, useLocation} from "react-router-dom";
import Swal from "sweetalert2";
import CurrencyFormat from "react-currency-format";
import UploadImage from "../public/img/uploadImage2.png";

import {updateInfoUsers} from "../redux/actions/authActions";

const FormInfoAkunComponent = () => {
    const dispatch = useDispatch();
    const location = useLocation();
    const {isAuthenticated, user, status} = useSelector((state) => state.auth);

    const [fotoUser, setFotoUser] = useState("");

    const imgPreview = (e) => {
        if (e.target.files[0]) {
            const reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("fotoUser-preview").src = event.target.result;
            };
            reader.readAsDataURL(e.target.files[0]);
            setFotoUser(e.target.files[0]);
        } else {
            document.getElementById("fotoUser-preview").src = UploadImage;
            setFotoUser("");
        }
    };

    React.useEffect(() => {
        if (location.pathname === "/info-akun") cekUserInfo();
    });

    function cekUserInfo() {
        if (!isAuthenticated) {
            return <Navigate to="/login" />;
        } else {
            if (user !== null && status !== "UPDATE_SUCCESS") {
                if (user.nama !== null) document.getElementById("nama").value = user.nama;
                if (user.kota !== null) document.getElementById("kota").value = user.kota;
                if (user.alamat !== null) document.getElementById("alamat").value = user.alamat;
                if (user.noHp !== null) document.getElementById("noHp").value = user.noHp;
                if (user.fotoUser !== null) document.getElementById("fotoUser-preview").src = user.fotoUser;
            }
        }
    }

    const handleSubmit = async (e) => {
        const updateArgs = {
            nama: document.getElementById("nama").value,
            kota: document.getElementById("kota").value,
            alamat: document.getElementById("alamat").value,
            noHp: document.getElementById("noHp").value,
            fotoUser,
        };

        if (updateArgs.nama === "" || updateArgs.kota === "" || updateArgs.alamat === "" || updateArgs.noHp === "") {
            Swal.fire({
                title: "Error",
                text: "Semua field harus diisi",
                icon: "error",
                confirmButtonText: "Ok",
            });
        } else {
            Swal.fire({
                title: "Data sudah benar ?",
                text: "Apakah anda yakin ingin menyimpan data ini ?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Ya, Simpan!",
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: "Loading",
                        text: "Mohon tunggu...",
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                        showConfirmButton: false,
                        showCloseButton: false,
                        showCancelButton: false,
                        showClass: {
                            popup: "animate__animated animate__fadeInDown",
                        },
                    });

                    dispatch(updateInfoUsers(updateArgs));
                }
            });
        }
    };

    if (status === "UPDATE_SUCCESS") {
        return <Navigate to={`/`} />;
    }
    return (
        <Container fluid>
            <Row className="justify-content-md-center mt-5">
                <Col lg={5} md={10} className="d-flex">
                    <div className="justify-content-end">
                        <Link to="/">
                            <i className="bi bi-arrow-left fs-4 d-flex align-items-center"></i>
                        </Link>
                    </div>
                    <div className="mx-auto">
                        <h4 className="d-flex align-items-center">Lengkapi Info Akun</h4>
                    </div>
                </Col>
            </Row>
            <Row className="justify-content-md-center">
                <Col lg={6}>
                    <Form className="formInfoAkun">
                        <Form.Group className="mb-3 text-center">
                            <div className="image-upload">
                                <label htmlFor="file-input" id="preview">
                                    <img id="fotoUser-preview" className="display-none uploadImgProfil m-2" src={UploadImage} alt="" />
                                </label>
                                <input id="file-input" name="fotoUser" type="file" onChange={imgPreview} required />
                            </div>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label className="formLabel">Nama</Form.Label>
                            <Form.Control type="text" className="formInput" placeholder="Nama Lengkap" id="nama" />
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label className="formLabel">Kota</Form.Label>
                            <Form.Control type="text" className="formInput" placeholder="Contoh: Jakarta" id="kota" />
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label className="formLabel">Alamat</Form.Label>
                            <Form.Control type="text" as="textarea" className="formInput" placeholder="Contoh: Jalan Ikan Hiu 33" id="alamat" />
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label className="formLabel">No Handphone (Terdaftar di Whatsapp)</Form.Label>
                            <CurrencyFormat prefix={"+62"} placeholder="+62" className="form-control formInput" id="noHp" maxLength={15} />
                        </Form.Group>
                        <Button type="button" className="btn-block w-100 mb-3 btnPrimary" onClick={() => handleSubmit()}>
                            Simpan
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
};

export default FormInfoAkunComponent;
