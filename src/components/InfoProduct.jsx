import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, Navigate, useParams, useNavigate } from "react-router-dom";

import { Container, Row, Col, Button, Stack } from "react-bootstrap";
import Swal from "sweetalert2";
import CurrencyFormat from "react-currency-format";

import CarouselPreviewProduct from "./CarouselPreviewProduct";
import { deleteProduct, getProductById, clearProduct } from "../redux/actions/productsActions";

const InfoProductComponent = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { user } = useSelector((state) => state.auth);
    const { id } = useParams();
    const { detailProduct, status } = useSelector((state) => state.product);

    React.useEffect(() => {
        if (detailProduct.length === 0) dispatch(getProductById(id));
    }, [detailProduct, dispatch, id]);

    function handleDelete() {
        const oldImage = [];
        if (detailProduct.fotoProduk[0] !== undefined) {
            oldImage.push(detailProduct.fotoProduk[0].substring(65, 85));
        }
        if (detailProduct.fotoProduk[1] !== undefined) {
            oldImage.push(detailProduct.fotoProduk[1].substring(65, 85));
        }
        if (detailProduct.fotoProduk[2] !== undefined) {
            oldImage.push(detailProduct.fotoProduk[2].substring(65, 85));
        }
        if (detailProduct.fotoProduk[3] !== undefined) {
            oldImage.push(detailProduct.fotoProduk[3].substring(65, 85));
        }

        Swal.fire({
            title: "Konfirmasi",
            text: "Apakah anda yakin ingin menghapus produk ini?",
            icon: "question",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Ya",
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire({
                    title: "Loading",
                    text: "Mohon tunggu...",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false,
                    showConfirmButton: false,
                    showCloseButton: false,
                    showCancelButton: false,
                    showClass: {
                        popup: "animate__animated animate__fadeInDown",
                    },
                });
                dispatch(deleteProduct({ id: detailProduct.id, oldImage }));
            }
        });
    }

    function handleEdit() {
        dispatch(clearProduct());
        return navigate(`/edit-produk/${id}`);
    }

    function handleBack() {
        dispatch(clearProduct());
    }

    if (status === "OK") {
        return <Navigate to={`/daftar-jual`} />;
    }
    return (
        <>
            {detailProduct.length === 0 ? (
                <></>
            ) : (
                <Container className="mt-5">
                    <Row className="justify-content-md-center mt-5 mb-4">
                        <Col lg={10} className="d-flex">
                            <div className="justify-content-start">
                                <Link to="/daftar-jual" onClick={handleBack}>
                                    <i className="bi bi-arrow-left fs-4 d-flex align-items-center"></i>
                                </Link>
                            </div>
                            <div className="mx-auto">
                                <h4 className="d-flex align-items-center">Info Produk</h4>
                            </div>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col lg={6} md={7} xs={12}>
                            {detailProduct.fotoProduk.length !== 0 ? <CarouselPreviewProduct img={detailProduct.fotoProduk} /> : <></>}
                            <div className="boxPreview">
                                <h5>Deskripsi</h5>
                                <p>{detailProduct.deskripsi}</p>
                            </div>
                        </Col>
                        <Col lg={4} md={5} xs={12}>
                            <div className="boxPreview mt-3">
                                <h5>{detailProduct.namaProduk}</h5>
                                <p>{detailProduct.kategori}</p>
                                <h5>
                                    <CurrencyFormat value={detailProduct.hargaProduk} displayType={"text"} thousandSeparator={"."} decimalSeparator={","} prefix={"Rp. "} />
                                </h5>
                                <Button type="button" onClick={() => handleEdit()} className="w-100 btnPrimary my-2">
                                    Edit
                                </Button>
                                <Button type="button" onClick={() => handleDelete()} variant="outline-danger" className="btnDanger w-100">
                                    Hapus
                                </Button>
                            </div>
                            <div className="boxPreview my-3">
                                {user === null ? (
                                    <></>
                                ) : (
                                    <Stack direction="horizontal" gap={3}>
                                        <img src={user.fotoUser} id="imgProfil" alt="" className="image-profile" />
                                        <div>
                                            <h5 className="my-auto">{user.nama}</h5>
                                            <p className="my-auto">{user.kota}</p>
                                        </div>
                                    </Stack>
                                )}
                            </div>
                        </Col>
                    </Row>
                </Container>
            )}
        </>
    );
};

export default InfoProductComponent;
