import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, Navigate } from "react-router-dom";

import { Container, Row, Col, Form, Button, Stack, Modal } from "react-bootstrap";

import Swal from "sweetalert2";
import UploadImage from "../public/img/uploadImage.png";
import CarouselPreviewProduct from "./CarouselPreviewProduct";
import CurrencyFormat from "react-currency-format";

import { addProduct, previewImg } from "../redux/actions/productsActions";

const FormAddProductComponent = () => {
    const dispatch = useDispatch();
    const { user, error } = useSelector((state) => state.auth);
    const { previewProduct, status } = useSelector((state) => state.product);

    const [namaProduk, setNamaProduk] = useState("");
    const [hargaProduk, setHargaProduk] = useState("");
    const [kategori, setKategori] = useState("");
    const [deskripsi, setDeskripsi] = useState("");
    const [fotoProduk1, setFotoProduk1] = useState("");
    const [fotoProduk2, setFotoProduk2] = useState("");
    const [fotoProduk3, setFotoProduk3] = useState("");
    const [fotoProduk4, setFotoProduk4] = useState("");

    useEffect(() => {
        if (error) {
            alert(error);
        }
    }, [error]);

    const imgTemp = [];

    const imgPreview1 = (e) => {
        if (e.target.files[0]) {
            const reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("img-preview1").src = event.target.result;
            };
            reader.readAsDataURL(e.target.files[0]);
            setFotoProduk1(e.target.files[0]);
        } else {
            document.getElementById("img-preview1").src = UploadImage;
            setFotoProduk1("");
        }
    };

    const imgPreview2 = (e) => {
        if (e.target.files[0]) {
            const reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("img-preview2").src = event.target.result;
            };
            reader.readAsDataURL(e.target.files[0]);
            setFotoProduk2(e.target.files[0]);
        } else {
            document.getElementById("img-preview2").src = UploadImage;
            setFotoProduk2("");
        }
    };

    const imgPreview3 = (e) => {
        if (e.target.files[0]) {
            const reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("img-preview3").src = event.target.result;
            };
            reader.readAsDataURL(e.target.files[0]);
            setFotoProduk3(e.target.files[0]);
        } else {
            document.getElementById("img-preview3").src = UploadImage;
            setFotoProduk3("");
        }
    };

    const imgPreview4 = (e) => {
        if (e.target.files[0]) {
            const reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("img-preview4").src = event.target.result;
            };
            reader.readAsDataURL(e.target.files[0]);
            setFotoProduk4(e.target.files[0]);
        } else {
            document.getElementById("img-preview4").src = UploadImage;
            setFotoProduk4("");
        }
    };

    const handleSubmit = async () => {
        if (namaProduk === "" || hargaProduk === "" || kategori === "" || deskripsi === "" || fotoProduk1 === "") {
            Swal.fire({
                title: "Error",
                text: "Semua field harus diisi",
                icon: "error",
                confirmButtonText: "Ok",
            });
        } else {
            Swal.fire({
                title: "Konfirmasi",
                text: "Apakah anda yakin ingin menambahkan produk ini?",
                icon: "question",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Ya",
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: "Loading",
                        text: "Mohon tunggu...",
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                        showConfirmButton: false,
                        showCloseButton: false,
                        showCancelButton: false,
                        showClass: {
                            popup: "animate__animated animate__fadeInDown",
                        },
                    });
                    let harga = parseInt(hargaProduk.replace(/[^0-9]/g, ""));
                    const createArgs = {
                        namaProduk,
                        harga,
                        kategori,
                        deskripsi,
                        fotoProduk: [fotoProduk1, fotoProduk2, fotoProduk3, fotoProduk4],
                    };
                    dispatch(addProduct(createArgs));
                }
            });
        }
    };

    const [fullscreen, setFullscreen] = useState(true);
    const [show, setShow] = useState(false);

    function handleShow() {
        if (namaProduk === "" || hargaProduk === "" || kategori === "" || deskripsi === "" || fotoProduk1 === "") {
            Swal.fire({
                title: "Error",
                text: "Semua field harus diisi",
                icon: "error",
                confirmButtonText: "Ok",
            });
        } else {
            imgTemp.splice(0, imgTemp.length);
            imgTemp.push(document.getElementById("img-preview1").src);
            if (document.getElementById("file-input2").value !== "") {
                imgTemp.push(document.getElementById("img-preview2").src);
            }
            if (document.getElementById("file-input3").value !== "") {
                imgTemp.push(document.getElementById("img-preview3").src);
            }
            if (document.getElementById("file-input4").value !== "") {
                imgTemp.push(document.getElementById("img-preview4").src);
            }
            dispatch(previewImg(imgTemp));

            setFullscreen(true);
            setShow(true);
        }
    }

    function handleClose() {
        setFullscreen(false);
        setShow(false);
    }

    if (status === "PRODUCT_CREATED") {
        return <Navigate to={`/daftar-jual`} />;
    }

    if (user !== null) {
        if (user.alamat === null) {
            Swal.fire({
                position: "center",
                icon: "warning",
                title: "Harap Lengkapi Info Akun",
                showConfirmButton: false,
                timer: 1000,
            });
            return <Navigate to="/info-akun" />;
        }
    }

    return (
        <Container>
            <Row className="justify-content-md-center mt-5 mb-3">
                <Col lg={8} className="d-flex">
                    <div className="justify-content-start">
                        <Link to="/daftar-jual">
                            <i className="bi bi-arrow-left fs-4 d-flex align-items-center"></i>
                        </Link>
                    </div>
                    <div className="mx-auto">
                        <h4 className="d-flex align-items-center">Lengkapi Detail Produk</h4>
                    </div>
                </Col>
            </Row>
            <Row className="justify-content-md-center mt-4">
                <Col lg={7}>
                    <Form>
                        <Form.Group className="mb-3" controlId="namaProduk">
                            <Form.Label className="formLabel">Nama Produk</Form.Label>
                            <Form.Control onChange={(e) => setNamaProduk(e.target.value)} required type="text" className="formInput" placeholder="Nama Produk" value={namaProduk} />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="hargaProduk">
                            <Form.Label className="formLabel">Harga Produk</Form.Label>
                            <CurrencyFormat
                                className="form-control formInput"
                                onChange={(e) => setHargaProduk(e.target.value)}
                                value={hargaProduk}
                                thousandSeparator={"."}
                                decimalSeparator={","}
                                prefix={"Rp. "}
                                placeholder="Rp 0.00"
                                required
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="hargaProduk">
                            <Form.Label className="formLabel">Kategori Produk</Form.Label>
                            <Form.Select onChange={(e) => setKategori(e.target.value)} required aria-label="Kategori Produk" className="formInput" value={kategori}>
                                <option>Pilih Kategori</option>
                                <option value="Hobi">Hobi</option>
                                <option value="Kendaraan">Kendaraan</option>
                                <option value="Baju">Baju</option>
                                <option value="Elektronik">Elektronik</option>
                                <option value="Kesehatan">Kesehatan</option>
                            </Form.Select>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="deskripsiProduk">
                            <Form.Label className="formLabel">Deskripsi</Form.Label>
                            <Form.Control
                                onChange={(e) => setDeskripsi(e.target.value)}
                                required
                                as="textarea"
                                rows={3}
                                className="formInput"
                                placeholder="Contoh: Jalan Ikan Hiu 33"
                                value={deskripsi}
                            />
                        </Form.Group>
                        <Form.Group controlId="formFile" className="mb-3">
                            <Form.Label className="formLabel">Foto Produk</Form.Label>
                            <div className="image-upload">
                                <label htmlFor="file-input1" id="preview">
                                    <img id="img-preview1" className="display-none image-preview m-2" src={UploadImage} alt="" />
                                </label>
                                <input id="file-input1" name="fotoProduk1" type="file" onChange={imgPreview1} accept="image/png, image/jpeg" />
                                <label htmlFor="file-input2" id="preview">
                                    <img id="img-preview2" className="display-none image-preview m-2" src={UploadImage} alt="" />
                                </label>
                                <input id="file-input2" name="fotoProduk2" type="file" onChange={imgPreview2} accept="image/png, image/jpeg" />
                                <label htmlFor="file-input3" id="preview">
                                    <img id="img-preview3" className="display-none image-preview m-2" src={UploadImage} alt="" />
                                </label>
                                <input id="file-input3" name="fotoProduk3" type="file" onChange={imgPreview3} accept="image/png, image/jpeg" />
                                <label htmlFor="file-input4" id="preview">
                                    <img id="img-preview4" className="display-none image-preview m-2" src={UploadImage} alt="" />
                                </label>
                                <input id="file-input4" name="fotoProduk4" type="file" onChange={imgPreview4} accept="image/png, image/jpeg" />
                            </div>
                        </Form.Group>
                        <Modal show={show} fullscreen={fullscreen} onHide={() => setShow(false)}>
                            <Modal.Header closeButton className="bgDark border-0">
                                <Modal.Title>Preview Product</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="bgDark">
                                <Container>
                                    <Row className="justify-content-md-center">
                                        <Col lg={6} md={7} xs={11}>
                                            <CarouselPreviewProduct img={previewProduct} />
                                            <div className="boxPreview">
                                                <h5>Deskripsi</h5>
                                                <p>{deskripsi}</p>
                                            </div>
                                        </Col>
                                        <Col lg={4} md={5} xs={11}>
                                            <div className="boxPreview">
                                                <h5>{namaProduk}</h5>
                                                <p>{kategori}</p>
                                                <h5>
                                                    <CurrencyFormat value={hargaProduk} displayType={"text"} thousandSeparator={"."} decimalSeparator={","} prefix={"Rp. "} />
                                                </h5>
                                                <Button type="button" onClick={() => handleSubmit()} className="btn-block w-100 btnPrimary my-2">
                                                    Terbitkan
                                                </Button>
                                                <Button onClick={handleClose} className="btn-block w-100 btnOutline my-1">
                                                    Edit
                                                </Button>
                                            </div>
                                            <div className="boxPreview mt-2">
                                                {user === null ? (
                                                    <></>
                                                ) : (
                                                    <Stack direction="horizontal" gap={3}>
                                                        <img src={user.fotoUser} id="imgProfil" alt="" className="image-profile" />
                                                        <div>
                                                            <h5 className="my-auto">{user.nama}</h5>
                                                            <p className="my-auto">{user.kota}</p>
                                                        </div>
                                                    </Stack>
                                                )}
                                            </div>
                                        </Col>
                                    </Row>
                                </Container>
                            </Modal.Body>
                        </Modal>
                        <Stack direction="horizontal" gap={3}>
                            <Button className="btn-block w-50 btnOutline" onClick={() => handleShow()}>
                                Preview
                            </Button>
                            <Button type="button" onClick={() => handleSubmit()} className="btn-block w-50 btnPrimary">
                                Terbitkan
                            </Button>
                        </Stack>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
};

export default FormAddProductComponent;
