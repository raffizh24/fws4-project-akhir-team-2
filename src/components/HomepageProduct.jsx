import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {getProductById} from "../redux/actions/productsActions";

import {Container, Card, Col, Row} from "react-bootstrap";
import {Link} from "react-router-dom";
import CurrencyFormat from "react-currency-format";

const CardProduct = () => {
    const dispatch = useDispatch();
    const {product} = useSelector((state) => state.product);

    const handleSubmit = (id) => {
        dispatch(getProductById(id));
    };

    return (
        <Container>
            <Row>
                {product.length === 0 ? (
                    <>
                        <h4 className="text-center pt-5">Produk Tidak Tersedia</h4>
                    </>
                ) : (
                    product.map((product) => (
                        <Col key={product.id} lg={2} md={3} xs={6} className="my-2">
                            <Link to={`/product/${product.id}`}>
                                <Card className="cardProduct" onClick={() => handleSubmit(product.id)}>
                                    <Card.Img variant="top" src={product.fotoProduk[0]} className="imgProduct" />
                                    <Card.Body>
                                        <Card.Title style={{fontSize: "14px", height: "10px"}}>{product.namaProduk}</Card.Title>
                                        <Card.Text style={{fontSize: "12px", height: "8px"}}>{product.kategori}</Card.Text>
                                        <Card.Text style={{fontSize: "14px", height: "12px"}}>
                                            <CurrencyFormat value={product.hargaProduk} displayType={"text"} thousandSeparator={"."} decimalSeparator={","} prefix={"Rp. "} />
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Link>
                        </Col>
                    ))
                )}
            </Row>
        </Container>
    );
};

export default CardProduct;
