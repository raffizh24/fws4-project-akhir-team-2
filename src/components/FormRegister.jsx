import React, {useState, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useGoogleLogin} from "@react-oauth/google";
import {Container, Row, Col, Form, Button, Stack} from "react-bootstrap";
import CoverAuth from "../public/img/CoverAuth.png";
import {Link, Navigate} from "react-router-dom";
import Swal from "sweetalert2";

import {loginWithGoogle, registerViaForm, clear} from "../redux/actions/authActions";

const FormRegisterComponent = () => {
    const dispatch = useDispatch();
    const {isAuthenticated, status, error} = useSelector((state) => state.auth);

    useEffect(() => {
        if (error) {
            alert(error);
        }
    }, [error]);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [passwordConfirm, setPasswordConfirm] = useState("");
    const [nama, setNama] = useState("");

    const handleSubmit = async (e) => {
        if (email === "") {
            Swal.fire({
                position: "top-end",
                icon: "warning",
                title: "Please enter your email",
                showConfirmButton: false,
                timer: 1000,
            });
        } else if (password === "") {
            Swal.fire({
                position: "top-end",
                icon: "warning",
                title: "Please enter your password",
                showConfirmButton: false,
                timer: 1000,
            });
        } else if (nama === "") {
            Swal.fire({
                position: "top-end",
                icon: "warning",
                title: "Please enter your name",
                showConfirmButton: false,
                timer: 1000,
            });
        } else if (password !== passwordConfirm) {
            Swal.fire({
                position: "center",
                icon: "warning",
                title: "Password doesn't match",
                showConfirmButton: false,
                timer: 1000,
            });
        } else {
            Swal.fire({
                title: "Loading",
                text: "Please wait...",
                showConfirmButton: false,
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
            });
            dispatch(registerViaForm({email, password, nama}));
        }
    };

    const googleLogin = useGoogleLogin({
        onSuccess: async (tokenResponse) => {
            dispatch(loginWithGoogle(tokenResponse.access_token));
        },
        onError: (error) => {
            alert(error);
        },
    });

    if (status === "REGISTER_SUCCESS") {
        dispatch(clear());
        return <Navigate to={`/login`} />;
    }

    return (
        <>
            {!isAuthenticated ? (
                <Container fluid>
                    <Row className="h-100 align-items-center">
                        <Col lg={6} className="m-0 p-0 cover-image">
                            <img src={CoverAuth} className="img-fluid image-login" alt="" />
                        </Col>
                        <Col lg={6}>
                            <Form className="formAuth">
                                <h3 className="fw-bold mb-3">Daftar</h3>
                                <Form.Group className="mb-3" controlId="nama">
                                    <Form.Label className="formLabel">Nama</Form.Label>
                                    <Form.Control type="text" className="formInput" placeholder="Nama Lengkap" value={nama} onChange={(e) => setNama(e.target.value)} />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="email">
                                    <Form.Label className="formLabel">Email</Form.Label>
                                    <Form.Control type="email" className="formInput" placeholder="Contoh: johndee@gmail.com" value={email} onChange={(e) => setEmail(e.target.value)} />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="password">
                                    <Form.Label className="formLabel">Password</Form.Label>
                                    <Form.Control type="password" className="formInput" placeholder="Masukkan password" value={password} onChange={(e) => setPassword(e.target.value)} />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="password2">
                                    <Form.Label className="formLabel">Konfirmasi Password</Form.Label>
                                    <Form.Control type="password" className="formInput" placeholder="Masukkan password" value={passwordConfirm} onChange={(e) => setPasswordConfirm(e.target.value)} />
                                </Form.Group>
                                <Button type="button" className="btn-block w-100 mb-3 btnPrimary" onClick={() => handleSubmit()}>
                                    Daftar
                                </Button>
                                <Button type="button" onClick={() => googleLogin()} className="btn-block w-100 btnOutline">
                                    <i className="bi bi-google me-2"></i>Sign Up with Google
                                </Button>
                                <div className="mt-3 d-flex justify-content-center">
                                    <Stack direction="horizontal" gap={1}>
                                        <p>Sudah punya akun?</p>
                                        <Link to="/login">
                                            <p style={{color: "#4b1979", fontWeight: "bold"}}>Masuk di sini</p>
                                        </Link>
                                    </Stack>
                                </div>
                            </Form>
                        </Col>
                    </Row>
                </Container>
            ) : (
                <Navigate to="/" />
            )}
        </>
    );
};

export default FormRegisterComponent;
