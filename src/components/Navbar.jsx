import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Link, useNavigate} from "react-router-dom";
import SimpleDateTime from "react-simple-timestamp-to-date";
import CurrencyFormat from "react-currency-format";

import {Container, Navbar, Nav, Button, Offcanvas, Stack, Dropdown, InputGroup, Form, Badge} from "react-bootstrap";
import {logout, cekTokenExp} from "../redux/actions/authActions";
import Brand from "../public/img/Brand.png";

import {getProductByNama, getAllProducts, clearProduct} from "../redux/actions/productsActions";
import {getTransactionSeller, getTransactionBuyer} from "../redux/actions/transactionsActions";
import {changeTheme} from "../redux/actions/themeActions";

const NavbarComponent = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const {isAuthenticated, user, status} = useSelector((state) => state.auth);
    const {transactionSeller, transactionBuyer} = useSelector((state) => state.transaction);
    const [theme, setTheme] = React.useState("");
    let totalTrBuyer = 0;
    let filterTrBuyer = 0;

    useEffect(() => {
        if (localStorage.getItem("token") && status !== "EXPIRED") {
            dispatch(cekTokenExp());
            dispatch(getTransactionSeller());
            dispatch(getTransactionBuyer());
        }
        if (status === "EXPIRED") return navigate("/login");
        if (localStorage.getItem("theme") === "dark") {
            setTheme(true);
        } else {
            setTheme(false);
        }
    }, [dispatch, isAuthenticated, status, navigate]);

    setTimeout(() => {
        if (localStorage.getItem("token")) dispatch(cekTokenExp());
    }, 5 * 60000);

    if (localStorage.getItem("token") && status !== "EXPIRED") {
        if (transactionBuyer !== undefined) {
            filterTrBuyer = transactionBuyer.filter((tr) => tr.status !== "Selesai" && tr.status !== "Ditolak");
            totalTrBuyer = filterTrBuyer.length;
        }
    }

    const handleSubmit = async () => {
        let keyword = document.getElementById("form-search").value;
        if (keyword === "") {
            dispatch(getAllProducts());
        } else {
            dispatch(getProductByNama(keyword));
        }
    };

    const handleKeyPress = async (target) => {
        if (target.charCode === 13) handleSubmit();
    };

    const handleLogout = () => {
        dispatch(logout());
        dispatch(clearProduct());
        dispatch(getAllProducts());
    };

    const themeMode = () => {
        if (document.getElementById("theme-mode").checked === true) {
            setTheme(true);
            dispatch(changeTheme("dark"));
        } else {
            setTheme(false);
            dispatch(changeTheme("light"));
        }
    };

    return (
        <div className="navbar-component py-1">
            {["md"].map((expand) => (
                <Navbar key={expand} expand={expand} className="py-1">
                    <Container>
                        <Navbar.Brand>
                            <Link to="/">
                                <img src={Brand} alt="" />
                            </Link>
                        </Navbar.Brand>

                        <Navbar.Toggle className="toggle-navbar" aria-controls={`offcanvasNavbar-expand-${expand}`} />
                        <Navbar.Offcanvas id={`offcanvasNavbar-expand-${expand}`} placement="end">
                            <Offcanvas.Header closeButton className="bgDark">
                                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>Second Hand</Offcanvas.Title>
                            </Offcanvas.Header>
                            <Offcanvas.Body className="bgDark">
                                <Nav className="justify-content-end flex-grow-1 align-items-start ">
                                    {!user ? (
                                        <>
                                            {/* Search Box */}
                                            <div className="search-box me-auto">
                                                <InputGroup>
                                                    <Form.Control id="form-search" placeholder="Cari di sini ..." className="search-box" onKeyPress={handleKeyPress} />
                                                    <Button type="submit" variant="light" id="button-addon2" className="search-box" onClick={handleSubmit}>
                                                        <i className="bi bi-search"></i>
                                                    </Button>
                                                </InputGroup>
                                            </div>
                                            <div className="my-auto me-3">
                                                <input type="checkbox" className="checkboxTheme" id="theme-mode" onChange={themeMode} checked={theme} />
                                                <label htmlFor="theme-mode" className="labelTheme">
                                                    <i className="bi bi-moon"></i>
                                                    <i className="bi bi-sun"></i>
                                                    <div className="ballTheme" />
                                                </label>
                                            </div>
                                            {/* Login */}
                                            <div className="fw-bold">
                                                <Link to="/login">
                                                    <Button className="btnPrimary border-0">
                                                        <i className="bi bi-box-arrow-in-right me-1" /> Masuk
                                                    </Button>
                                                </Link>
                                            </div>
                                        </>
                                    ) : (
                                        <>
                                            {/* Search Box */}
                                            <div className="search-box me-auto divSearch my-auto">
                                                <InputGroup>
                                                    <Form.Control id="form-search" placeholder="Cari di sini ..." className="search-box" onKeyPress={handleKeyPress} />
                                                    <Button type="submit" variant="light" id="button-addon2" className="search-box " onClick={handleSubmit}>
                                                        <i className="bi bi-search"></i>
                                                    </Button>
                                                </InputGroup>
                                            </div>
                                            <div className="my-auto me-3 toggle-dark">
                                                <input type="checkbox" className="checkboxTheme" id="theme-mode" onChange={themeMode} checked={theme} />
                                                <label htmlFor="theme-mode" className="labelTheme">
                                                    <i className="bi bi-moon"></i>
                                                    <i className="bi bi-sun"></i>
                                                    <div className="ballTheme" />
                                                </label>
                                            </div>
                                            {/* Menu */}
                                            <Dropdown>
                                                <Dropdown.Toggle id="dropdown-basic" className="navbar-icon">
                                                    <i className="bi bi-list-ul fs-3"></i>
                                                </Dropdown.Toggle>
                                                <Dropdown.Menu className="dropdown-menu">
                                                    <Dropdown.Item onClick={() => navigate("/")}>Beranda</Dropdown.Item>
                                                    <Dropdown.Item onClick={() => navigate("/daftar-jual")}>Daftar Jual</Dropdown.Item>
                                                </Dropdown.Menu>
                                            </Dropdown>
                                            {/* Notification */}
                                            {transactionSeller === undefined || transactionBuyer === undefined ? (
                                                <></>
                                            ) : (
                                                <Dropdown>
                                                    <Dropdown.Toggle id="dropdown-basic" className="navbar-icon">
                                                        <i className="bi bi-bell fs-3"></i>
                                                        {transactionSeller.length + filterTrBuyer.length === 0 ? (
                                                            <></>
                                                        ) : (
                                                            <Badge
                                                                pill
                                                                className=""
                                                                style={{
                                                                    position: "absolute",
                                                                    top: "5px",
                                                                    right: "10px",
                                                                    fontSize: "60%",
                                                                }}
                                                                bg="danger"
                                                            >
                                                                {transactionSeller.length + totalTrBuyer}
                                                            </Badge>
                                                        )}
                                                    </Dropdown.Toggle>
                                                    <Dropdown.Menu className="dropdown-menu">
                                                        <Dropdown.Item onClick={() => navigate("/transaksi")}>Transaksi</Dropdown.Item>
                                                        {transactionSeller.map((item, index) => {
                                                            return (
                                                                <Dropdown.Item key={`modal${index}`} onClick={() => navigate("/transaksi")}>
                                                                    <Stack direction="horizontal" gap={3}>
                                                                        <img src={item.Product.fotoProduk[0]} alt="" className="imageSmall align-self-start mt-1" />
                                                                        <div>
                                                                            <p className="my-auto" style={{fontSize: "10px", color: "#BABABA"}}>
                                                                                Penjualan Produk
                                                                            </p>
                                                                            <h5 className="my-auto" style={{fontSize: "12px", lineHeight: "22px"}}>
                                                                                {item.Product.namaProduk}
                                                                            </h5>
                                                                            <h5 className="my-auto" style={{fontSize: "12px", lineHeight: "22px"}}>
                                                                                <CurrencyFormat
                                                                                    value={item.Product.hargaProduk}
                                                                                    displayType={"text"}
                                                                                    thousandSeparator={"."}
                                                                                    decimalSeparator={","}
                                                                                    prefix={"Rp. "}
                                                                                />
                                                                            </h5>
                                                                            <h5 className="my-auto" style={{fontSize: "12px", lineHeight: "22px"}}>
                                                                                Ditawar{" "}
                                                                                <CurrencyFormat
                                                                                    value={item.penawaran}
                                                                                    displayType={"text"}
                                                                                    thousandSeparator={"."}
                                                                                    decimalSeparator={","}
                                                                                    prefix={"Rp. "}
                                                                                />
                                                                            </h5>
                                                                        </div>
                                                                        <p className="align-self-start ms-auto" style={{fontSize: "12px", color: "#BABABA"}}>
                                                                            <SimpleDateTime dateSeparator="-" format="MYD" showTime="0">
                                                                                {item.createdAt}
                                                                            </SimpleDateTime>
                                                                        </p>
                                                                    </Stack>
                                                                </Dropdown.Item>
                                                            );
                                                        })}
                                                        {filterTrBuyer.map((item, index) => {
                                                            return (
                                                                <Dropdown.Item key={`modal${index}`} onClick={() => navigate("/transaksi")}>
                                                                    <Stack direction="horizontal" gap={3}>
                                                                        <img src={item.Product.fotoProduk[0]} alt="" className="imageSmall align-self-start mt-1" />
                                                                        <div>
                                                                            <p className="my-auto" style={{fontSize: "10px", color: "#BABABA"}}>
                                                                                Pembelian Produk
                                                                            </p>
                                                                            <h5 className="my-auto" style={{fontSize: "12px", lineHeight: "22px"}}>
                                                                                {item.Product.namaProduk}
                                                                            </h5>
                                                                            <h5 className="my-auto" style={{fontSize: "12px", lineHeight: "22px"}}>
                                                                                <CurrencyFormat
                                                                                    value={item.Product.hargaProduk}
                                                                                    displayType={"text"}
                                                                                    thousandSeparator={"."}
                                                                                    decimalSeparator={","}
                                                                                    prefix={"Rp. "}
                                                                                />
                                                                            </h5>
                                                                            <h5 className="my-auto" style={{fontSize: "12px", lineHeight: "22px"}}>
                                                                                Ditawar{" "}
                                                                                <CurrencyFormat
                                                                                    value={item.penawaran}
                                                                                    displayType={"text"}
                                                                                    thousandSeparator={"."}
                                                                                    decimalSeparator={","}
                                                                                    prefix={"Rp. "}
                                                                                />
                                                                            </h5>
                                                                        </div>
                                                                        <p className="align-self-start ms-auto" style={{fontSize: "12px", color: "#BABABA"}}>
                                                                            <SimpleDateTime dateSeparator="-" format="MYD" showTime="0">
                                                                                {item.createdAt}
                                                                            </SimpleDateTime>
                                                                        </p>
                                                                    </Stack>
                                                                </Dropdown.Item>
                                                            );
                                                        })}
                                                    </Dropdown.Menu>
                                                </Dropdown>
                                            )}
                                            {/* Profile */}
                                            <Dropdown>
                                                <Dropdown.Toggle id="dropdown-basic" className="navbar-icon">
                                                    <i className="bi bi-person fs-3"></i>
                                                </Dropdown.Toggle>
                                                <Dropdown.Menu className="dropdown-menu">
                                                    <Dropdown.Item className="dropdown-item" onClick={() => navigate("/info-akun")}>
                                                        Akun Saya
                                                    </Dropdown.Item>
                                                    <Dropdown.Item onClick={handleLogout}>Logout</Dropdown.Item>
                                                </Dropdown.Menu>
                                            </Dropdown>
                                        </>
                                    )}
                                </Nav>
                            </Offcanvas.Body>
                        </Navbar.Offcanvas>
                    </Container>
                </Navbar>
            ))}
        </div>
    );
};

export default NavbarComponent;
