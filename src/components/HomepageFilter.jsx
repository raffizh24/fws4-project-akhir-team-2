import React from "react";
import {useDispatch, useSelector} from "react-redux";

import {Container, Button} from "react-bootstrap";

import {getAllProducts, getProductByKategori, clearProduct} from "../redux/actions/productsActions";

const FilterComponent = () => {
    const dispatch = useDispatch();
    const {product, status} = useSelector((state) => state.product);

    React.useEffect(() => {
        dispatch(getAllProducts());
        dispatch(clearProduct());
    }, [dispatch]);

    if (product.length === 0 && status !== "GET_ALL") {
        dispatch(getAllProducts());
    }

    const filterAll = (event) => {
        event.currentTarget.classList.remove("btnFilterOff");
        event.currentTarget.classList.add("btnFilterOn");
        document.getElementById("filterHobi").classList.remove("btnFilterOn");
        document.getElementById("filterHobi").classList.add("btnFilterOff");
        document.getElementById("filterKendaraan").classList.remove("btnFilterOn");
        document.getElementById("filterKendaraan").classList.add("btnFilterOff");
        document.getElementById("filterBaju").classList.remove("btnFilterOn");
        document.getElementById("filterBaju").classList.add("btnFilterOff");
        document.getElementById("filterElektronik").classList.remove("btnFilterOn");
        document.getElementById("filterElektronik").classList.add("btnFilterOff");
        document.getElementById("filterKesehatan").classList.remove("btnFilterOn");
        document.getElementById("filterKesehatan").classList.add("btnFilterOff");
        dispatch(getAllProducts());
    };

    const filterHobi = (event) => {
        let kategori = "Hobi";
        event.currentTarget.classList.remove("btnFilterOff");
        event.currentTarget.classList.add("btnFilterOn");
        document.getElementById("filterAll").classList.remove("btnFilterOn");
        document.getElementById("filterAll").classList.add("btnFilterOff");
        document.getElementById("filterKendaraan").classList.remove("btnFilterOn");
        document.getElementById("filterKendaraan").classList.add("btnFilterOff");
        document.getElementById("filterBaju").classList.remove("btnFilterOn");
        document.getElementById("filterBaju").classList.add("btnFilterOff");
        document.getElementById("filterElektronik").classList.remove("btnFilterOn");
        document.getElementById("filterElektronik").classList.add("btnFilterOff");
        document.getElementById("filterKesehatan").classList.remove("btnFilterOn");
        document.getElementById("filterKesehatan").classList.add("btnFilterOff");
        dispatch(getProductByKategori(kategori));
    };

    const filterKendaraan = (event) => {
        let kategori = "Kendaraan";
        event.currentTarget.classList.remove("btnFilterOff");
        event.currentTarget.classList.add("btnFilterOn");
        document.getElementById("filterHobi").classList.remove("btnFilterOn");
        document.getElementById("filterHobi").classList.add("btnFilterOff");
        document.getElementById("filterAll").classList.remove("btnFilterOn");
        document.getElementById("filterAll").classList.add("btnFilterOff");
        document.getElementById("filterBaju").classList.remove("btnFilterOn");
        document.getElementById("filterBaju").classList.add("btnFilterOff");
        document.getElementById("filterElektronik").classList.remove("btnFilterOn");
        document.getElementById("filterElektronik").classList.add("btnFilterOff");
        document.getElementById("filterKesehatan").classList.remove("btnFilterOn");
        document.getElementById("filterKesehatan").classList.add("btnFilterOff");
        dispatch(getProductByKategori(kategori));
    };

    const filterBaju = (event) => {
        let kategori = "Baju";
        event.currentTarget.classList.remove("btnFilterOff");
        event.currentTarget.classList.add("btnFilterOn");
        document.getElementById("filterHobi").classList.remove("btnFilterOn");
        document.getElementById("filterHobi").classList.add("btnFilterOff");
        document.getElementById("filterKendaraan").classList.remove("btnFilterOn");
        document.getElementById("filterKendaraan").classList.add("btnFilterOff");
        document.getElementById("filterAll").classList.remove("btnFilterOn");
        document.getElementById("filterAll").classList.add("btnFilterOff");
        document.getElementById("filterElektronik").classList.remove("btnFilterOn");
        document.getElementById("filterElektronik").classList.add("btnFilterOff");
        document.getElementById("filterKesehatan").classList.remove("btnFilterOn");
        document.getElementById("filterKesehatan").classList.add("btnFilterOff");
        dispatch(getProductByKategori(kategori));
    };

    const filterElektronik = (event) => {
        let kategori = "Elektronik";
        event.currentTarget.classList.remove("btnFilterOff");
        event.currentTarget.classList.add("btnFilterOn");
        document.getElementById("filterHobi").classList.remove("btnFilterOn");
        document.getElementById("filterHobi").classList.add("btnFilterOff");
        document.getElementById("filterKendaraan").classList.remove("btnFilterOn");
        document.getElementById("filterKendaraan").classList.add("btnFilterOff");
        document.getElementById("filterBaju").classList.remove("btnFilterOn");
        document.getElementById("filterBaju").classList.add("btnFilterOff");
        document.getElementById("filterAll").classList.remove("btnFilterOn");
        document.getElementById("filterAll").classList.add("btnFilterOff");
        document.getElementById("filterKesehatan").classList.remove("btnFilterOn");
        document.getElementById("filterKesehatan").classList.add("btnFilterOff");
        dispatch(getProductByKategori(kategori));
    };

    const filterKesehatan = (event) => {
        let kategori = "Kesehatan";
        event.currentTarget.classList.remove("btnFilterOff");
        event.currentTarget.classList.add("btnFilterOn");
        document.getElementById("filterHobi").classList.remove("btnFilterOn");
        document.getElementById("filterHobi").classList.add("btnFilterOff");
        document.getElementById("filterKendaraan").classList.remove("btnFilterOn");
        document.getElementById("filterKendaraan").classList.add("btnFilterOff");
        document.getElementById("filterBaju").classList.remove("btnFilterOn");
        document.getElementById("filterBaju").classList.add("btnFilterOff");
        document.getElementById("filterElektronik").classList.remove("btnFilterOn");
        document.getElementById("filterElektronik").classList.add("btnFilterOff");
        document.getElementById("filterAll").classList.remove("btnFilterOn");
        document.getElementById("filterAll").classList.add("btnFilterOff");
        dispatch(getProductByKategori(kategori));
    };

    return (
        <Container>
            <div className="pt-2 d-flex justify-content-between">
                <h4 className="h4 fw-bold">Telusuri Kategori</h4>
            </div>
            <div className="py-3 d-flex justify-content-start scroll">
                <Button className="me-3 btnFilterOn" onClick={filterAll} id="filterAll">
                    <i className="bi bi-search"></i> Semua
                </Button>
                <Button className="me-3 btnFilterOff" onClick={filterHobi} id="filterHobi">
                    <i className="bi bi-search"></i> Hobi
                </Button>
                <Button className="me-3 btnFilterOff" onClick={filterKendaraan} id="filterKendaraan">
                    <i className="bi bi-search"></i> Kendaraan
                </Button>
                <Button className="me-3 btnFilterOff" onClick={filterBaju} id="filterBaju">
                    <i className="bi bi-search"></i> Baju
                </Button>
                <Button className="me-3 btnFilterOff" onClick={filterElektronik} id="filterElektronik">
                    <i className="bi bi-search"></i> Elektronik
                </Button>
                <Button className="me-3 btnFilterOff" onClick={filterKesehatan} id="filterKesehatan">
                    <i className="bi bi-search"></i> Kesehatan
                </Button>
            </div>
        </Container>
    );
};

export default FilterComponent;
