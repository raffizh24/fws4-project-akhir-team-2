import React, {useState, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Link, Navigate, useNavigate, useParams} from "react-router-dom";
import Swal from "sweetalert2";
import CurrencyFormat from "react-currency-format";

import CarouselPreviewProduct from "./CarouselPreviewProduct";
import {Container, Row, Col, Button, Stack, Modal, Form} from "react-bootstrap";

import {getProductById} from "../redux/actions/productsActions";
import {createTransaction} from "../redux/actions/transactionsActions";

const BuyerInfoProductComponent = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const {id} = useParams();
    const {user} = useSelector((state) => state.auth);
    const {detailProduct} = useSelector((state) => state.product);
    const {status} = useSelector((state) => state.transaction);

    useEffect(() => {
        dispatch(getProductById(id));
    }, [dispatch, id]);

    const [negotiation, setNegotiation] = useState("");
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleSubmit = (e) => {
        const {nama, kota, alamat, fotoUser} = user;
        if (nama === null || kota === null || alamat === null || fotoUser === null) {
            Swal.fire({
                position: "center",
                icon: "warning",
                title: "Harap Lengkapi Info Akun",
                showConfirmButton: false,
                timer: 1000,
            });
            return navigate("/info-akun");
        } else if (negotiation === "") {
            Swal.fire({
                position: "top-end",
                icon: "warning",
                title: "Harap isi nilai negosiasi",
                showConfirmButton: false,
                timer: 1000,
            });
        } else {
            Swal.fire({
                position: "center",
                icon: "warning",
                title: "Apakah anda yakin?",
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
            }).then((result) => {
                if (result.value) {
                    let data = {
                        idBuyer: user.id,
                        idProduk: detailProduct.id,
                        penawaran: parseInt(negotiation.replace(/[^0-9]/g, "")),
                        status: "Menunggu",
                    };
                    Swal.fire({
                        title: "Loading",
                        text: "Please wait...",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    dispatch(createTransaction(data));
                }
            });
        }
    };

    if (status === "OK") return <Navigate to={`/`} />;

    return (
        <>
            {detailProduct.length === 0 ? (
                <></>
            ) : (
                <Container className="mt-5">
                    <Row className="justify-content-md-center mt-5 mb-4">
                        <Col lg={10} className="d-flex">
                            <div className="justify-content-start">
                                <Link to="/">
                                    <i className="bi bi-arrow-left fs-4 d-flex align-items-center"></i>
                                </Link>
                            </div>
                        </Col>
                    </Row>
                    <Row className="justify-content-md-center">
                        <Col lg={6} md={7} xs={12}>
                            {detailProduct.fotoProduk.length !== 0 ? <CarouselPreviewProduct img={detailProduct.fotoProduk} /> : <></>}
                            <div className="boxPreview">
                                <h5>Deskripsi</h5>
                                <p>{detailProduct.deskripsi}</p>
                            </div>
                        </Col>
                        <Col lg={4} md={5} xs={12}>
                            <div className="boxPreview mt-3">
                                <h5>{detailProduct.namaProduk}</h5>
                                <p>{detailProduct.kategori}</p>
                                <h5>
                                    <CurrencyFormat value={detailProduct.hargaProduk} displayType={"text"} thousandSeparator={"."} decimalSeparator={","} prefix={"Rp. "} />
                                </h5>
                                <>
                                    <Button type="button" onClick={handleShow} className="w-100 btnPrimary my-2">
                                        Saya tertarik dan ingin nego
                                    </Button>
                                    <Modal show={show} onHide={handleClose} centered contentClassName="custom-modal">
                                        <Modal.Header variant="Header" className="modalHeader" closeButton></Modal.Header>
                                        <Modal.Body className="judul">Masukan tawaranmu di sini!</Modal.Body>
                                        <Modal.Body>
                                            Harga tawaranmu akan diketahui penual, jika penjual cocok kamu akan segera dihubungi penjual.
                                            <Stack direction="horizontal" gap={3} className="infoPenjual mt-3">
                                                <img src={detailProduct.fotoProduk[0]} alt="" className="image-profile" />
                                                <div>
                                                    <h5 className="my-auto">{detailProduct.namaProduk}</h5>
                                                    <p className="my-auto mt-2">
                                                        <CurrencyFormat value={detailProduct.hargaProduk} displayType={"text"} thousandSeparator={"."} decimalSeparator={","} prefix={"Rp. "} />
                                                    </p>
                                                </div>
                                            </Stack>
                                            <Form className="nego">
                                                <Form.Group className="mb-3" controlId="">
                                                    <Form.Label className="formLabel formLabelNego">Harga Tawar</Form.Label>
                                                    <CurrencyFormat
                                                        className="form-control formInput formNego"
                                                        placeholder="Rp xxx"
                                                        onChange={(e) => setNegotiation(e.target.value)}
                                                        thousandSeparator={"."}
                                                        decimalSeparator={","}
                                                        prefix={"Rp. "}
                                                    />
                                                </Form.Group>
                                            </Form>
                                        </Modal.Body>
                                        <Modal.Footer className="modalFooter">
                                            <Button className="btnNego" variant="primary" onClick={handleSubmit}>
                                                Kirim
                                            </Button>
                                        </Modal.Footer>
                                    </Modal>
                                </>
                            </div>
                            <div className="boxPreview my-3">
                                <Stack direction="horizontal" gap={3}>
                                    <img src={detailProduct.User.fotoUser} id="imgProfil" alt="" className="image-profile" />
                                    <div>
                                        <h5 className="my-auto">{detailProduct.User.nama}</h5>
                                        <p className="my-auto">{detailProduct.User.kota}</p>
                                    </div>
                                </Stack>
                            </div>
                        </Col>
                    </Row>
                </Container>
            )}
        </>
    );
};

export default BuyerInfoProductComponent;
