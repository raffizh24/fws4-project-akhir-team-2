import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Link, Navigate} from "react-router-dom";
import {useGoogleLogin} from "@react-oauth/google";

import {Container, Row, Col, Form, Button, Stack} from "react-bootstrap";
import CoverAuth from "../public/img/CoverAuth.png";
import Swal from "sweetalert2";

import {loginWithGoogle, loginViaForm} from "../redux/actions/authActions";

const FormLoginComponent = () => {
    const dispatch = useDispatch();
    const {isAuthenticated} = useSelector((state) => state.auth);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (email === "") {
            Swal.fire({
                position: "top-end",
                icon: "warning",
                title: "Please enter your email",
                showConfirmButton: false,
                timer: 1000,
            });
        }
        if (password === "") {
            Swal.fire({
                position: "top-end",
                icon: "warning",
                title: "Password cannot be empty",
                showConfirmButton: false,
                timer: 1000,
            });
        }
        if (email !== "" && password !== "") {
            Swal.fire({
                title: "Loading",
                text: "Please wait...",
                showConfirmButton: false,
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
            });
            dispatch(loginViaForm({email, password}));
        }
    };

    const googleLogin = useGoogleLogin({
        onSuccess: async (tokenResponse) => {
            Swal.fire({
                title: "Loading",
                text: "Please wait...",
                showConfirmButton: false,
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
            });
            dispatch(loginWithGoogle(tokenResponse.access_token));
        },
        onError: (error) => {
            alert(error);
        },
    });

    return (
        <>
            {!isAuthenticated ? (
                <Container fluid>
                    <Row className="h-100 align-items-center">
                        <Col lg={6} className="m-0 p-0 cover-image">
                            <img src={CoverAuth} className="img-fluid image-login" alt="" />
                        </Col>
                        <Col lg={6}>
                            <Form className="formAuth" onSubmit={handleSubmit}>
                                <h3 className="fw-bold mb-3">Masuk</h3>
                                <Form.Group className="mb-3" controlId="email">
                                    <Form.Label className="formLabel">Email</Form.Label>
                                    <Form.Control type="email" className="formInput" placeholder="Contoh: johndee@gmail.com" value={email} onChange={(e) => setEmail(e.target.value)} />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="password">
                                    <Form.Label className="formLabel">Password</Form.Label>
                                    <Form.Control type="password" className="formInput" placeholder="Masukkan password" value={password} onChange={(e) => setPassword(e.target.value)} />
                                </Form.Group>
                                <Button type="submit" className="btn-block w-100 btnPrimary mb-3">
                                    Masuk
                                </Button>
                                <Button type="button" onClick={() => googleLogin()} className="btn-block w-100 btnOutline">
                                    <i className="bi bi-google me-2"></i>Sign In with Google
                                </Button>
                                <div className="mt-3 d-flex justify-content-center">
                                    <Stack direction="horizontal" gap={1}>
                                        <p>Belum punya akun?</p>
                                        <Link to="/register">
                                            <p style={{color: "#4b1979", fontWeight: "bold"}}>Daftar di sini</p>
                                        </Link>
                                    </Stack>
                                </div>
                            </Form>
                        </Col>
                    </Row>
                </Container>
            ) : (
                <Navigate to={`/`} />
            )}
        </>
    );
};

export default FormLoginComponent;
