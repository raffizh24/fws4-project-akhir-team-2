import React from "react";
import {Link} from "react-router-dom";
import {Container, Button, Row, Col} from "react-bootstrap";

const FooterComponent = () => {
    return (
        <footer className="my-3 fixed-bottom">
            <Container>
                <Row className="justify-content-center">
                    <Col md="auto" xs="auto">
                        <Link to="/tambah-produk">
                            <Button type="submit" className="btnPrimaryJual">
                                <i className="bi bi-plus"></i> Jual
                            </Button>
                        </Link>
                    </Col>
                </Row>
            </Container>
        </footer>
    );
};

export default FooterComponent;
