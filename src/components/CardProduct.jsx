import React from "react";
import { useDispatch } from "react-redux";
import { getProductById } from "../redux/actions/productsActions";
import CurrencyFormat from "react-currency-format";

import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";

const CardProductComponent = ({ product }) => {
    const dispatch = useDispatch();
    const { id, namaProduk, hargaProduk, kategori, fotoProduk } = product;

    const handleSubmit = (event) => {
        dispatch(getProductById(id));
    };

    return (
        <Link to={`/info-produk/${product.id}`}>
            <Card onClick={() => handleSubmit()} className="cardProductSmall">
                <Card.Img variant="top" src={fotoProduk[0]} className="imgProductLarge" />
                <Card.Body className="bgDark2">
                    <Card.Title style={{ fontSize: "14px", height: "10px" }}>{namaProduk}</Card.Title>
                    <Card.Text style={{ fontSize: "10px", height: "5px" }}>{kategori}</Card.Text>
                    <Card.Text style={{ fontSize: "14px", height: "12px" }}>
                        <CurrencyFormat value={hargaProduk} displayType={"text"} thousandSeparator={"."} decimalSeparator={","} prefix={"Rp. "} />
                    </Card.Text>
                </Card.Body>
            </Card>
        </Link>
    );
};

export default CardProductComponent;
