import React, {useState, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Link, Navigate, useParams} from "react-router-dom";

import {Container, Row, Col, Form, Button, Stack, Modal} from "react-bootstrap";
import Swal from "sweetalert2";
import UploadImage from "../public/img/uploadImage.png";
import ImageTest from "../public/img/imageTest.png";
import CarouselPreviewProduct from "./CarouselPreviewProduct";
import CurrencyFormat from "react-currency-format";

import {updateProduct, getProductById, previewImg, clearProduct} from "../redux/actions/productsActions";

const FormEditProductComponent = () => {
    const dispatch = useDispatch();
    const {id} = useParams();
    const {user} = useSelector((state) => state.auth);
    const {previewProduct, detailProduct, status} = useSelector((state) => state.product);

    const [idProduk, setIdProduk] = useState("");
    const [namaProduk, setNamaProduk] = useState("");
    const [hargaProduk, setHargaProduk] = useState("");
    const [kategori, setKategori] = useState("");
    const [deskripsi, setDeskripsi] = useState("");
    const [statusProduk, setStatusProduk] = useState("");
    const [fotoProduk1, setFotoProduk1] = useState("");
    const [fotoProduk2, setFotoProduk2] = useState("");
    const [fotoProduk3, setFotoProduk3] = useState("");
    const [fotoProduk4, setFotoProduk4] = useState("");
    const oldImage = [];

    useEffect(() => {
        dispatch(getProductById(id));
    }, [dispatch, id]);

    function cekProduk() {
        if (detailProduct.length !== 0) {
            if (idProduk === "") {
                setIdProduk(detailProduct.id);
                setNamaProduk(detailProduct.namaProduk);
                setHargaProduk(detailProduct.hargaProduk);
                setKategori(detailProduct.kategori);
                setDeskripsi(detailProduct.deskripsi);
                setStatusProduk(detailProduct.statusProduk);
                if (detailProduct.fotoProduk[0] !== undefined) setFotoProduk1(detailProduct.fotoProduk[0]);
                if (detailProduct.fotoProduk[1] !== undefined) setFotoProduk2(detailProduct.fotoProduk[1]);
                if (detailProduct.fotoProduk[2] !== undefined) setFotoProduk3(detailProduct.fotoProduk[2]);
                if (detailProduct.fotoProduk[3] !== undefined) setFotoProduk4(detailProduct.fotoProduk[3]);

                if (detailProduct.fotoProduk[3] !== undefined) {
                    document.getElementById("img-preview1").src = detailProduct.fotoProduk[0];
                    document.getElementById("img-preview2").src = detailProduct.fotoProduk[1];
                    document.getElementById("img-preview3").src = detailProduct.fotoProduk[2];
                    document.getElementById("img-preview4").src = detailProduct.fotoProduk[3];
                } else if (detailProduct.fotoProduk[2] !== undefined) {
                    document.getElementById("img-preview1").src = detailProduct.fotoProduk[0];
                    document.getElementById("img-preview2").src = detailProduct.fotoProduk[1];
                    document.getElementById("img-preview3").src = detailProduct.fotoProduk[2];
                } else if (detailProduct.fotoProduk[1] !== undefined) {
                    document.getElementById("img-preview1").src = detailProduct.fotoProduk[0];
                    document.getElementById("img-preview2").src = detailProduct.fotoProduk[1];
                } else if (detailProduct.fotoProduk[0] !== undefined) {
                    document.getElementById("img-preview1").src = detailProduct.fotoProduk[0];
                }
            }
        }
    }

    cekProduk();

    const imgTemp = ["", "", "", ""];

    const imgPreview1 = (e) => {
        if (e.target.files[0]) {
            const reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("img-preview1").src = event.target.result;
            };
            reader.readAsDataURL(e.target.files[0]);
            setFotoProduk1(e.target.files[0]);
        } else {
            document.getElementById("img-preview1").src = UploadImage;
            detailProduct.fotoProduk[0] !== undefined ? setFotoProduk1(detailProduct.fotoProduk[0]) : setFotoProduk1("");
        }
    };

    const imgPreview2 = (e) => {
        if (e.target.files[0]) {
            const reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("img-preview2").src = event.target.result;
            };
            reader.readAsDataURL(e.target.files[0]);
            setFotoProduk2(e.target.files[0]);
        } else {
            document.getElementById("img-preview2").src = UploadImage;
            detailProduct.fotoProduk[1] !== undefined ? setFotoProduk2(detailProduct.fotoProduk[1]) : setFotoProduk2("");
        }
    };

    const imgPreview3 = (e) => {
        if (e.target.files[0]) {
            const reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("img-preview3").src = event.target.result;
            };
            reader.readAsDataURL(e.target.files[0]);
            setFotoProduk3(e.target.files[0]);
        } else {
            document.getElementById("img-preview3").src = UploadImage;
            detailProduct.fotoProduk[2] !== undefined ? setFotoProduk3(detailProduct.fotoProduk[2]) : setFotoProduk3("");
        }
    };

    const imgPreview4 = (e) => {
        if (e.target.files[0]) {
            const reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("img-preview4").src = event.target.result;
            };
            reader.readAsDataURL(e.target.files[0]);
            setFotoProduk4(e.target.files[0]);
        } else {
            document.getElementById("img-preview4").src = UploadImage;
            detailProduct.fotoProduk[3] !== undefined ? setFotoProduk4(detailProduct.fotoProduk[3]) : setFotoProduk4("");
        }
    };

    const handleSubmit = async (e) => {
        if ((fotoProduk1.type === "image/jpeg" || fotoProduk1.type === "image/png") && detailProduct.fotoProduk[0] !== undefined) {
            oldImage.push(detailProduct.fotoProduk[0].substring(65, 85));
        }
        if ((fotoProduk2.type === "image/jpeg" || fotoProduk2.type === "image/png") && detailProduct.fotoProduk[1] !== undefined) {
            oldImage.push(detailProduct.fotoProduk[1].substring(65, 85));
        }
        if ((fotoProduk3.type === "image/jpeg" || fotoProduk3.type === "image/png") && detailProduct.fotoProduk[2] !== undefined) {
            oldImage.push(detailProduct.fotoProduk[2].substring(65, 85));
        }
        if ((fotoProduk4.type === "image/jpeg" || fotoProduk4.type === "image/png") && detailProduct.fotoProduk[3] !== undefined) {
            oldImage.push(detailProduct.fotoProduk[3].substring(65, 85));
        }
        let newHarga;
        if (hargaProduk !== detailProduct.hargaProduk) {
            newHarga = hargaProduk.replace(/[^0-9]/g, "");
        } else {
            newHarga = detailProduct.hargaProduk;
        }

        const updateArgs = {
            id: detailProduct.id,
            idSeller: user.id,
            namaProduk,
            hargaProduk: newHarga,
            kategori,
            deskripsi,
            statusProduk,
            fotoProduk: [fotoProduk1, fotoProduk2, fotoProduk3, fotoProduk4],
            oldImage,
        };
        Swal.fire({
            title: "Konfirmasi",
            text: "Apakah anda yakin ingin mengubah produk ini?",
            icon: "question",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Ya",
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire({
                    title: "Loading",
                    text: "Mohon tunggu...",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false,
                    showConfirmButton: false,
                    showCloseButton: false,
                    showCancelButton: false,
                    showClass: {
                        popup: "animate__animated animate__fadeInDown",
                    },
                });
                dispatch(updateProduct(updateArgs));
            }
        });
    };

    const [fullscreen, setFullscreen] = useState(true);
    const [show, setShow] = useState(false);

    function handleShow() {
        imgTemp.splice(0, imgTemp.length);
        imgTemp.push(document.getElementById("img-preview1").src);
        if (document.getElementById("img-preview2").src !== UploadImage) {
            imgTemp.push(document.getElementById("img-preview2").src);
        }
        if (document.getElementById("img-preview3").src !== UploadImage) {
            imgTemp.push(document.getElementById("img-preview3").src);
        }
        if (document.getElementById("img-preview4").src !== UploadImage) {
            imgTemp.push(document.getElementById("img-preview4").src);
        }

        namaProduk === "" ? setNamaProduk(detailProduct.namaProduk) : setNamaProduk(namaProduk);
        kategori === "" ? setKategori(detailProduct.kategori) : setKategori(kategori);
        deskripsi === "" ? setDeskripsi(detailProduct.deskripsi) : setDeskripsi(deskripsi);
        statusProduk === "" ? setStatusProduk(detailProduct.statusProduk) : setStatusProduk(statusProduk);

        dispatch(previewImg(imgTemp));
        setFullscreen(true);
        setShow(true);
    }

    function handleClose() {
        setFullscreen(false);
        setShow(false);
    }

    function handleBack() {
        dispatch(clearProduct());
    }

    if (status === "OK") {
        return <Navigate to={`/daftar-jual`} />;
    }

    return (
        <Container>
            <Row className="justify-content-md-center mt-5 mb-3">
                <Col lg={8} className="d-flex">
                    <div className="justify-content-start">
                        <Link to="/daftar-jual" onClick={handleBack}>
                            <i className="bi bi-arrow-left fs-4 d-flex align-items-center"></i>
                        </Link>
                    </div>
                    <div className="mx-auto">
                        <h4 className="d-flex align-items-center">Ubah Detail Produk</h4>
                    </div>
                </Col>
            </Row>
            <Row className="justify-content-md-center mt-4">
                <Col lg={7}>
                    <Form encType="multipart/form-data">
                        <Form.Control type="text" id="idProduk" hidden />
                        <Form.Group className="mb-3">
                            <Form.Label className="formLabel">Nama Produk</Form.Label>
                            <Form.Control value={namaProduk} onChange={(e) => setNamaProduk(e.target.value)} required type="text" className="formInput" placeholder="Nama Produk" id="namaProduk" />
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label className="formLabel">Harga Produk</Form.Label>
                            <CurrencyFormat
                                className="form-control formInput"
                                onChange={(e) => setHargaProduk(e.target.value)}
                                value={hargaProduk}
                                thousandSeparator={"."}
                                decimalSeparator={","}
                                prefix={"Rp. "}
                                placeholder="Rp 0.00"
                                required
                            />
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label className="formLabel">Kategori Produk</Form.Label>
                            <Form.Select value={kategori} onChange={(e) => setKategori(e.target.value)} required aria-label="Kategori Produk" className="formInput" id="kategori">
                                <option>Pilih Kategori</option>
                                <option value="Hobi">Hobi</option>
                                <option value="Kendaraan">Kendaraan</option>
                                <option value="Baju">Baju</option>
                                <option value="Elektronik">Elektronik</option>
                                <option value="Kesehatan">Kesehatan</option>
                            </Form.Select>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label className="formLabel">Deskripsi</Form.Label>
                            <Form.Control
                                value={deskripsi}
                                onChange={(e) => setDeskripsi(e.target.value)}
                                required
                                as="textarea"
                                rows={3}
                                className="formInput"
                                placeholder="Contoh: Jalan Ikan Hiu 33"
                                id="deskripsi"
                            />
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label className="formLabel">Status Produk</Form.Label>
                            <Form.Select value={statusProduk} onChange={(e) => setStatusProduk(e.target.value)} required aria-label="Status Produk" className="formInput" id="statusProduk">
                                <option>Pilih Status</option>
                                <option value="Tersedia">Tersedia</option>
                                <option value="Terjual">Terjual</option>
                            </Form.Select>
                        </Form.Group>
                        <Form.Group controlId="formFile" className="mb-3">
                            <Form.Label className="formLabel">Foto Produk</Form.Label>
                            <div className="image-upload">
                                <label htmlFor="file-input1" id="preview">
                                    <img id="img-preview1" className="display-none image-preview m-2" src={UploadImage} alt="" />
                                </label>
                                <input id="file-input1" name="fotoProduk1" type="file" onChange={imgPreview1} required />

                                <label htmlFor="file-input2" id="preview">
                                    <img id="img-preview2" className="display-none image-preview m-2" src={UploadImage} alt="" />
                                </label>
                                <input id="file-input2" name="fotoProduk2" type="file" onChange={imgPreview2} />

                                <label htmlFor="file-input3" id="preview">
                                    <img id="img-preview3" className="display-none image-preview m-2" src={UploadImage} alt="" />
                                </label>
                                <input id="file-input3" name="fotoProduk3" type="file" onChange={imgPreview3} />

                                <label htmlFor="file-input4" id="preview">
                                    <img id="img-preview4" className="display-none image-preview m-2" src={UploadImage} alt="" />
                                </label>
                                <input id="file-input4" name="fotoProduk4" type="file" onChange={imgPreview4} />
                            </div>
                        </Form.Group>
                        <Modal show={show} fullscreen={fullscreen} onHide={() => setShow(false)}>
                            <Modal.Header closeButton className="bgDark border-0">
                                <Modal.Title>Preview Product</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="bgDark">
                                <Container>
                                    <Row className="justify-content-md-center">
                                        <Col lg={6} md={7} xs={11}>
                                            <CarouselPreviewProduct img={previewProduct} />
                                            <div className="boxPreview">
                                                <h5>Deskripsi</h5>
                                                <p>{deskripsi}</p>
                                            </div>
                                        </Col>
                                        <Col lg={4} md={5} xs={11}>
                                            <div className="boxPreview">
                                                <h5>{namaProduk}</h5>
                                                <p>{kategori}</p>
                                                <h5>
                                                    <CurrencyFormat value={hargaProduk} displayType={"text"} thousandSeparator={"."} decimalSeparator={","} prefix={"Rp. "} />
                                                </h5>
                                                <Button type="button" onClick={() => handleSubmit()} className="btn-block w-100 btnPrimary my-2">
                                                    Terbitkan
                                                </Button>
                                                <Button onClick={handleClose} className="btn-block w-100 btnOutline my-1">
                                                    Edit
                                                </Button>
                                            </div>
                                            <div className="boxPreview mt-2">
                                                <Stack direction="horizontal" gap={3}>
                                                    <img src={ImageTest} alt="" className="image-profile" />
                                                    <div>
                                                        <h5 className="my-auto">Nama Penjual</h5>
                                                        <p className="my-auto">Kota</p>
                                                    </div>
                                                </Stack>
                                            </div>
                                        </Col>
                                    </Row>
                                </Container>
                            </Modal.Body>
                        </Modal>
                        <Stack direction="horizontal" gap={3}>
                            <Button className="btn-block w-50 btnOutline" onClick={() => handleShow()}>
                                Preview
                            </Button>
                            <Button type="button" onClick={() => handleSubmit()} className="btn-block w-50 btnPrimary">
                                Terbitkan
                            </Button>
                        </Stack>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
};

export default FormEditProductComponent;
