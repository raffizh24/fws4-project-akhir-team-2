/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Link, useNavigate, Navigate} from "react-router-dom";
import Swal from "sweetalert2";

import {Container, Row, Col, Button, Stack, Table} from "react-bootstrap";

import AddProduct from "../public/img/addProduct.png";
import CardProductComponent from "./CardProduct";
import {getProductByStatus, clearProduct} from "../redux/actions/productsActions";

const DaftarJualComponent = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const {user, error} = useSelector((state) => state.auth);
    const {product, status} = useSelector((state) => state.product);
    let idSeller = "";
    let statusProduk = "All";

    useEffect(() => {
        if (user !== null) {
            idSeller = user.id;
            statusProduk = "All";
            dispatch(clearProduct());
            dispatch(getProductByStatus({idSeller, statusProduk}));
        }
    }, [error]);

    if (product.length === 0 && user !== null && status !== "GET_ALL") {
        dispatch(getProductByStatus({idSeller: user.id, statusProduk: "All"}));
    }

    // Produk Tidak Difilter
    const filterAll = (event) => {
        idSeller = user.id;
        statusProduk = "All";
        event.currentTarget.classList.remove("kategoriInActive");
        event.currentTarget.classList.add("kategoriActive");
        document.getElementById("filterTersedia").classList.remove("kategoriActive");
        document.getElementById("filterTersedia").classList.add("kategoriInActive");
        document.getElementById("filterDiminati").classList.remove("kategoriActive");
        document.getElementById("filterDiminati").classList.add("kategoriInActive");
        document.getElementById("filterTerjual").classList.remove("kategoriActive");
        document.getElementById("filterTerjual").classList.add("kategoriInActive");
        dispatch(getProductByStatus({idSeller, statusProduk}));
    };

    // Filter Produk Tersedia
    const filterTersedia = (event) => {
        idSeller = user.id;
        statusProduk = "Tersedia";
        event.currentTarget.classList.remove("kategoriInActive");
        event.currentTarget.classList.add("kategoriActive");
        document.getElementById("filterAll").classList.remove("kategoriActive");
        document.getElementById("filterAll").classList.add("kategoriInActive");
        document.getElementById("filterDiminati").classList.remove("kategoriActive");
        document.getElementById("filterDiminati").classList.add("kategoriInActive");
        document.getElementById("filterTerjual").classList.remove("kategoriActive");
        document.getElementById("filterTerjual").classList.add("kategoriInActive");
        dispatch(getProductByStatus({idSeller, statusProduk}));
    };

    // Filter Produk Diminati
    const filterDiminati = (event) => {
        idSeller = user.id;
        statusProduk = "Diminati";
        event.currentTarget.classList.remove("kategoriInActive");
        event.currentTarget.classList.add("kategoriActive");
        document.getElementById("filterAll").classList.remove("kategoriActive");
        document.getElementById("filterAll").classList.add("kategoriInActive");
        document.getElementById("filterTersedia").classList.remove("kategoriActive");
        document.getElementById("filterTersedia").classList.add("kategoriInActive");
        document.getElementById("filterTerjual").classList.remove("kategoriActive");
        document.getElementById("filterTerjual").classList.add("kategoriInActive");
        dispatch(getProductByStatus({idSeller, statusProduk}));
    };

    // Filter Produk Terjual
    const filterTerjual = (event) => {
        idSeller = user.id;
        statusProduk = "Terjual";
        event.currentTarget.classList.remove("kategoriInActive");
        event.currentTarget.classList.add("kategoriActive");
        document.getElementById("filterAll").classList.remove("kategoriActive");
        document.getElementById("filterAll").classList.add("kategoriInActive");
        document.getElementById("filterTersedia").classList.remove("kategoriActive");
        document.getElementById("filterTersedia").classList.add("kategoriInActive");
        document.getElementById("filterDiminati").classList.remove("kategoriActive");
        document.getElementById("filterDiminati").classList.add("kategoriInActive");
        dispatch(getProductByStatus({idSeller, statusProduk}));
    };

    if (user !== null) {
        if (user.alamat === null) {
            Swal.fire({
                position: "center",
                icon: "warning",
                title: "Harap Lengkapi Info Akun",
                showConfirmButton: false,
                timer: 1000,
            });
            return <Navigate to="/info-akun" />;
        }
    }

    return {product} ? (
        <Container>
            <Row className="justify-content-md-center mt-5 mb-3">
                <Col>
                    <h4 className="fw-bold">Daftar Jual Saya</h4>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Stack direction="horizontal" gap={3} className="infoPenjual bgDark2">
                        {user === null ? (
                            <></>
                        ) : (
                            <>
                                <img src={user.fotoUser} alt="" className="image-profile" />
                                <div>
                                    <h5 className="my-auto">{user.nama}</h5>
                                    <p className="my-auto">{user.kota}</p>
                                </div>
                            </>
                        )}
                        <Button type="button" className="btn-block btnOutlineSmall me-2 ms-auto" onClick={() => navigate("/info-akun")}>
                            Edit
                        </Button>
                    </Stack>
                </Col>
            </Row>
            <Row>
                <Col lg={3} md={12} xs={12}>
                    <div className="boxShadow mt-4 bgDark2">
                        <h5>Kategori</h5>
                        <Table style={{color: "grey"}}>
                            <thead>
                                <tr style={{height: "50px"}} className="kategoriActive" id="filterAll" onClick={filterAll}>
                                    <td>
                                        <i className="bi bi-box me-2"></i>Semua Produk<i className="bi bi-chevron-right float-end"></i>
                                    </td>
                                </tr>
                                <tr style={{height: "50px"}} className="kategoriInActive" id="filterTersedia" onClick={filterTersedia}>
                                    <td>
                                        <i className="bi bi-archive me-2"></i>Tersedia<i className="bi bi-chevron-right float-end"></i>
                                    </td>
                                </tr>
                                <tr style={{height: "50px"}} className="kategoriInActive" id="filterDiminati" onClick={filterDiminati}>
                                    <td>
                                        <i className="bi bi-heart me-2"></i>Diminati<i className="bi bi-chevron-right float-end"></i>
                                    </td>
                                </tr>
                                <tr style={{height: "50px"}} className="kategoriInActive" id="filterTerjual" onClick={filterTerjual}>
                                    <td>
                                        <i className="bi bi-currency-dollar me-2"></i>Terjual<i className="bi bi-chevron-right float-end"></i>
                                    </td>
                                </tr>
                            </thead>
                        </Table>
                    </div>
                </Col>
                <Col lg={9} md={12} xs={12}>
                    <Row className="mt-4">
                        <Col lg={4} md={4} xs={6} className="mb-4">
                            <Link to="/tambah-produk">
                                <img src={AddProduct} className="imgBtnAdd" alt="" />
                            </Link>
                        </Col>
                        {product.length === 0 || product.length === undefined ? (
                            <></>
                        ) : (
                            product.map((product) => (
                                <Col key={product.id} lg={4} md={4} xs={6} className="mb-4">
                                    <CardProductComponent product={product} />
                                </Col>
                            ))
                        )}
                    </Row>
                </Col>
            </Row>
        </Container>
    ) : (
        <>
            {Swal.fire({
                title: "Loading",
                text: "Mohon tunggu sebentar",
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showConfirmButton: false,
                showCancelButton: false,
            })}
        </>
    );
};

export default DaftarJualComponent;
