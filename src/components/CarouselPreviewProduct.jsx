import React from "react";
import {Carousel} from "react-bootstrap";

const CardProductComponent = ({img}) => {
    return (
        <Carousel className="boxCarousel">
            {img.map((item, index) => {
                return (
                    <Carousel.Item key={index}>
                        <img className="d-block w-100 boxImagePreview" src={item} alt="First slide" />
                    </Carousel.Item>
                );
            })}
        </Carousel>
    );
};

export default CardProductComponent;
