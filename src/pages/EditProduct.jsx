import FormEditProduct from "../components/FormEditProduct";
import Navbar from "../components/Navbar";

function EditProduct() {
    return (
        <div className="App">
            <Navbar />
            <FormEditProduct />
        </div>
    );
}

export default EditProduct;
