import TransactionComponent from "../components/Transaksi";
import Navbar from "../components/Navbar";

function Transaksi() {
    return (
        <div className="App">
            <Navbar />
            <TransactionComponent />
        </div>
    );
}

export default Transaksi;
