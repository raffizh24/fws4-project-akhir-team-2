import Navbar from "../components/Navbar";
import BuyerInfoProductComponent from "../components/BuyerInfoProduct";

function HalamanProduct() {
    return (
        <div className="App">
            <Navbar />
            <BuyerInfoProductComponent />
        </div>
    );
}

export default HalamanProduct;
