import InfoAkunComponent from "../components/InfoAkun";
import Navbar from "../components/Navbar";

function InfoAkun() {
    return (
        <div className="App">
            <Navbar />
            <InfoAkunComponent />
        </div>
    );
}

export default InfoAkun;
