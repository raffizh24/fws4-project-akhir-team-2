import FormAddProduct from "../components/FormAddProduct";
import Navbar from "../components/Navbar";

function AddProduct() {
    return (
        <div className="App">
            <Navbar />
            <FormAddProduct />
        </div>
    );
}

export default AddProduct;
