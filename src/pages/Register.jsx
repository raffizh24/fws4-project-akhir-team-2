import RegisterForm from "../components/FormRegister";

function Register() {
    return (
        <div className="App">
            <RegisterForm />
        </div>
    );
}

export default Register;
