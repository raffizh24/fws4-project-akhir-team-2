import Navbar from "../components/Navbar";
import HomepageCarousel from "../components/HomepageCarousel";
import HomepageFilter from "../components/HomepageFilter";
import HomepageProduct from "../components/HomepageProduct";
import Footer from "../components/HomepageFooter";

function Homepage() {
    return (
        <div className="App">
            <Navbar />
            <HomepageCarousel />
            <HomepageFilter />
            <HomepageProduct />
            <Footer />
        </div>
    );
}

export default Homepage;
