import LoginForm from "../components/FormLogin";

function Login() {
    return (
        <div className="App">
            <LoginForm />
        </div>
    );
}

export default Login;
