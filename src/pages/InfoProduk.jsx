import InfoProductComponent from "../components/InfoProduct";
import Navbar from "../components/Navbar";

function InfoProduct() {
    return (
        <div className="App">
            <Navbar />
            <InfoProductComponent />
        </div>
    );
}

export default InfoProduct;
