import DaftarJual from "../components/DaftarJual";
import Navbar from "../components/Navbar";

function DaftarJualSaya() {
    return (
        <div className="App">
            <Navbar />
            <DaftarJual />
        </div>
    );
}

export default DaftarJualSaya;
